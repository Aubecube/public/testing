package fr.minepod.testing;

import java.util.Optional;
import java.util.UUID;

import fr.minepod.mpcore.APIVersion;
import fr.minepod.mpcore.api.commands.CommandType;
import fr.minepod.mpcore.api.messages.FancyChat;
import fr.minepod.mpcore.api.plugins.MPCorePluginBase;

public class Testing extends MPCorePluginBase {
  // Allows MPCore to check if the plugin is compiled against the latest API version available
  @Override
  public APIVersion getAPIVersion() {
    return APIVersion.ENGELBERG;
  }

  public Object dummy;

  @Override
  public void onEnableBefore() {
    // Use this.commands and this.dependencies to avoid confusion

    // Register commands
    // _____________________↓ description _______________↓ name ("/plugin_name command_name")
    this.commands.register("Voir la version du plugin", "version", CommandType.USER,
        // ________________________________________________________↑ default permission
        // _________________________________________________________ (plugin_name.user)
        (sender, command, label, args, processed) -> {
          FancyChat.create().normal("Version").highlight(getDescription().getVersion())
              .send(sender);
        });

    this.commands.register("Voir l'information", "info", CommandType.ADMINISTRATOR,
        (sender, command, label, args, processed) -> {
          // Check if a dependency is available when the command "/testing info" is called
          Optional<Boolean> available = this.dependencies.isAvailable("RegionForSale");
          if (!available.isPresent() || !available.get()) {
            // Easy chat formatting, puts automatic spaces between strings.
            // Possible methods: append, raw (= append with no space before), normal, success,
            // error, value, highlight
            FancyChat.create().error("RegionForSale n'est pas chargé.").send(sender);
            FancyChat.create().normal("Vous devriez vérifier le").highlight("lien Bukkit")
                .normal(".").send(sender);
          } else {
            FancyChat.create().success("Le plugin").value("RegionForSale")
                .success("est correctement chargé.").send(sender);
          }
        });

    // Supported type names are: int, float (dot separator), double (dot separator), long, uuid,
    // boolean ("true"/"false"), player (only online players), chain (of characters, must be the
    // last argument), string (if invalid or none specified)
    // _______________________________________________↓ ____________________↓ example with uuid
    this.commands.register("Divers", "divers [texte] [type=variable_typée] [uuid=UUID_spécifié]",
        // ↑ Help text will be "/testing divers [texte] [variable typée] [UUID spécifié]"
        CommandType.MODERATOR, (sender, command, label, args, processed) -> {
          // Automatic casting of argument 1
          String text = processed.get(1).getStringValue();
          UUID uuid = processed.get(3).getUuidValue();

          // 1: divers -> string (constant)
          // 2: texte -> string (variable)
          // 3: variable typée -> typed (variable)
          // 4: UUID spécifié -> UUID (variable)
        });

    // Register dynamic dependencies
    this.dependencies.put("plugin_name", () -> {
      // Code to execute when the plugin (plugin_name) is loaded and enabled
      dummy = (Object) getServer().getPluginManager().getPlugin("plugin_name");
    });
  }

  @Override
  public void onEnableAfter() {
    // Command to execute AFTER the automatic data loader
  }

  @Override
  public void onDisableBefore() {
    // Command to execute BEFORE saving all data (automatic)
  }

  @Override
  public void onDisableAfter() {
    // Command to execute AFTER the automatic data saver
  }
}
